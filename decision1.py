import pandas as pd
from sklearn.model_selection import train_test_split  
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor  
from sklearn.metrics import classification_report  

# Read csv-file from disk into dataframe object
df = pd.read_csv('data/cars.csv', sep=';', encoding='utf-8')
print(df.head())

# Convert non-numeric columns
cleanup_nums = {"Sex": {"M": 1, "F": 0},
                "Car": {"Sedan": 0, "Station Wagon": 1, "SUV": 2, "MPV": 3, "Sportscar": 4 }}
df.replace(cleanup_nums, inplace=True)
print(df.head())

# Split the dataset vertically, so that we have the column we are predicting in y and the data in X   
y = df['Car'] 
X = df.drop('Car', axis=1)  
X = X.drop('Price', axis=1)  
print(X.head())

# We split the dataset horizontally. We use 20% for testing and 80% for training
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

# Grow tree, using max_depth and/or min_impurity_decrease (gini) to manipulate depth
classifier = DecisionTreeClassifier(min_impurity_decrease=0.01, max_depth=5)  
classifier.fit(X_train, y_train)  

# Test tree
y_pred = classifier.predict(X_test) 
print(classification_report(y_test, y_pred))  

# Display tree
dotfile = open("./dtree1.dot", 'w')
tree.export_graphviz(classifier, out_file = dotfile, feature_names = X.columns, class_names=['Sedan','STW','SUV','MPV','Sport'])
dotfile.close()

# Visualize on http://webgraphviz.com/
# Or install graphviz or pydot (Google for help)

#######################################
# Predict price using a regression tree

# Split the dataset vertically, so that we have the column we are predicting in y and the data in X   
y = df['Price'] 
X = df.drop('Price', axis=1)  
X = X.drop('Car', axis=1)  
print(X.head())

# We split the dataset horizontally. We use 20% for testing and 80% for training
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10)

# Grow tree, using max_depth and/or min_impurity_decrease (mse, Mean Squared Error)
regr = DecisionTreeRegressor(max_depth=6, min_impurity_decrease=20000 ** 2)  
regr.fit(X_train, y_train)  

# Test tree
y_pred = regr.predict(X_test) 

print("Testing score: " + format(regr.score(X_test,y_test)))

# Display tree
dotfile = open("./dtree2.dot", 'w')
tree.export_graphviz(regr, out_file = dotfile, feature_names = X.columns)
dotfile.close()
