from collections import Counter
import pandas as pd
import numpy as np 
from matplotlib import pyplot as plt 


# display simple bar-chart
x = range(3)
y =  [2, 6, 5]
plt.bar(x, y, 0.9)
plt.xticks(x, ('A', 'B', 'C'))
plt.show()

# Read csv-file from disk into dataframe object
df = pd.read_csv('data/us-500.csv', sep=',')
print(df.shape)
print(df.head())

# Count number of entries per state
labels, values = zip(*Counter(df['state'].values).items())
x = range(len(labels))

# Set size, create plot and display plot
plt.figure(figsize=(15,7)) # Make the plot bigger
plt.bar(x, values, 0.9) # Create the plot with a little space between the bars
plt.xticks(x, labels)
plt.show()

# Create simple histogram with hard-coded values
data = np.array([1,2,3,4,6,7]) # Create ndarray with data
plt.hist(data, bins=3, range=(1, 10))
plt.show()

# Read csv-file from disk into dataframe object
df = pd.read_csv('data/ssb-sq-prices.csv', sep=';', decimal=',', encoding='iso8859-1')

# We only want data rows with category price per square meter
df = df[df.Variable == "Kvadratmeterpris (kr)"]
df = df.drop('Variable', axis=1) # We can drop this column now  
df = df[df['Value'].notnull()]

# Put values from Value-column into ndarray
data = df['Value'].values 

# Make plot
plt.hist(data, bins=50, range=(0, 60000)) # We remove 0-values
plt.show()

# Make super simple line-chart
plt.plot([1,2,3,4], [1,6,5,16])
plt.axis([0, 6, 0, 20])
plt.show()

# Make line-diagram with real data
df = df[df.Type == "01 Nye eneboliger"] # Filter: only "eneboliger"
df = df.drop('Type', axis=1)  # We can drop this column now  
print(df.head()) # View the current top 5 entries

# Make two different plots, with two different filters
df1 = df[df.Region == "02 Akershus"] # Filter: only "Akershus"
df2 = df[df.Region == "20 Finnmark - Finnmárku"] # Filter: only "Finnmark"

# Make 2 line-plots on same visualizations (they will be coloured separately)
plt.plot(df1['Date'].values, df1['Value'].values, label='Akershus')
plt.plot(df2['Date'].values, df2['Value'].values, label='Finnmark')
plt.legend() # Plot legends (the two labels)
plt.xlabel('Year') # Set x-axis text
plt.ylabel('Value') # Set y-axis text
plt.show() # Display plot

# Statistics
print("")
print("STATISTICS")
print("")

# Mean
mean = np.mean(data)
print("Mean: " + str(mean))

# Weighted average
average = np.average([2,3,4], weights=[0.5, 0.25, 0.25])
print("Weighted average: " + str(average))

# Median
median = np.median(data)
print("Median: " + str(median))

# Variance
print("Variance: " + str(np.var(data)))

# Standard deviation
print("Standard deviation: " + str(np.std(data)))

# Min/Max
print("Min: " + str(np.min(data)))
print("Max: " + str(np.max(data)))
