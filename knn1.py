import pandas as pd
from sklearn import neighbors
from matplotlib import pyplot as plt 
from io import StringIO

# Training dataset as a comma-separated string
csvTrain = StringIO("""Shape,X,Y
s,2,6
s,2.5,6.3
s,2.5,5.7
s,3,6.2
s,3,5.8
s,3.7,6.1
s,3.6,5.7
c,4,5.2
c,4.5,5.4
c,4.6,5.1
c,5.1,5.7
c,5.3,5
""")

# Load training dataset into dataframe
df = pd.read_csv(csvTrain, sep=',')
print(df.head())

# Plot circles from training set
circles = df[df.Shape == 'c'] # Filter: only rows where shape is 'c'ircle
plt.scatter(circles['X'], circles['Y'], marker='o')

# Plot squares from training set
squares = df[df.Shape == 's'] # Filter: only rows where shape is 's'quare
plt.scatter(squares['X'], squares['Y'], marker='s')

# Plot the point we are trying to predict as an X.
plt.scatter([4], [5], marker='x')

plt.show()

# Prepare training set by splitting it vertically into data and correct target values
trainX = df.drop('Shape', axis=1) # Drop 'Shape'-column, leaving columns 'X' and 'Y'
trainY = df['Shape'] # "Correct" target values

# Create KNN model
knn=neighbors.KNeighborsClassifier()
knn.fit(trainX, trainY)

predictData = pd.DataFrame({"X":[4], "Y":[5]})

# Make predictions
result = knn.predict(predictData)
print(result)