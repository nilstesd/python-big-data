import pandas as pd
from sklearn import manifold
from matplotlib import pyplot as plt 

# Read csv-file from disk into dataframe object
df = pd.read_csv('data/music-ratings.csv', sep=',', encoding='utf-8')
print(df.head())

# Reshape into wide dataframe (see result from print(df.head()) )
df = df.pivot_table(index='User', values='Rating', columns='Artist')
print(df.head())

# Compute euclidian distances between rows. Since this function uses columns, 
# we invert the table using T
df = df.T # Swap rows and columns
print(df.head())
distance = lambda column1, column2: pd.np.linalg.norm((column1 - column2).fillna(0))
distances = df.apply(lambda column2: df.apply(lambda column1: distance(column1, column2)))
print(distances.head())

# Make 2-dimensional projection of distances between users
mds = manifold.MDS(n_components=2, dissimilarity="precomputed", random_state=6)
results = mds.fit(distances)
coords = results.embedding_

# Plot points as circles
plt.subplots_adjust(bottom = 0.1)
plt.scatter(coords[:, 0], coords[:, 1], marker = 'o')

# Plot 'User' labels
for label, x, y in zip(df.columns.values, coords[:, 0], coords[:, 1]):
    plt.annotate(
        label,
        xy = (x, y), xytext = (-20, 20),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
plt.show()
 
# Find nearest neighbors by orderering by distance from "me"
me = 'Frank' # Assume 'Frank' needs new recommendations
neighbors = distances[me].drop(me, axis=0) # Remove "me" from neighbors

# Use only k-nearest neighbors
k = 2
orderedNeighbors = neighbors.sort_values() # Sort. Nearest neighbor first.
nearestNeighbors = orderedNeighbors[0:k] # Select the k first entries
print("My nearest neighbors:")
print(nearestNeighbors)

# Now we have found the nearest neighbors
# The rest of the code uses this to create recommendations based on these

# Replace nan's (not a number) with 0's to make pandas series addable
ratings = df
ratings = ratings.fillna(0)
print(ratings.head())
 
# Create a series with 0 ratings for each artist
rec = ratings[me] * 0

for i in nearestNeighbors.index:
    print("Finding recommendations from neighbor", i)
    # Find distance from me to this particular neighbor
    print("indexes:", me, i)
    dist = distances.at[me, i] 
      
    # Create a weight for this user based on the distance
    weight = 1 / (1 + dist)

    print("distance=", dist, "weight=", weight)
         
    # Find recommendations from this particular neighbor (ratings[i]),
    # multiply by weight
    # and add all values to recommendations
    # (each value in the 'i'-column of ratings is added to the series "rec")
    rec = rec + (ratings[i] * weight) 

# Order recommendations descending, getting highest recommendation first
rec = rec.sort_values(ascending=False) 
print(rec)